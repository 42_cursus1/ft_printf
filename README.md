# Ft_printf

The aim of this project is to recreate part of the C language printf function.
The mandatory part is to implement some of the specifiers: c s d i u p x X %
In the bonus part, we implement the flags (# +-0), and basic field width and basic precision (i.e. not with the * flag).
If you want to run this project, you will need to add the libft (a previous project, part of the same gitlab group) to the root of the folder.

This project taught me to handle structures, and helped me with getting more and more familiar with pointers.
It took me a while to get it done, also because I made the rookie mistake of digging into coding way too fast.
And... that also explains why the code is a bit of a mess, ahem... I would definitely do it differently today! 
But still I am proud of what I did, as I just could'nt code in C just a few months ago.

Oh and by the way, this was graded 125 by the moulinette!

